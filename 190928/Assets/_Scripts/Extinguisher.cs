﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Extinguisher : MonoBehaviour
{
    public Text ExtingText;
    static int ExtingScore = 0;

    public GameObject Dragon;
    static public GameObject Fire;

    static float FireDistance = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        Fire = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (Fire != null)
        {
            FireDistance = Vector3.Distance(Dragon.transform.position, Fire.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Exting") // 소화기 먹었을 때 
        {
            other.gameObject.SetActive(false);
            ExtingScore++;
            SetExtingText();
        }
        else if (other.tag == "Fire")
        {
            Debug.Log("Fire hit");
            Fire = other.gameObject;
        }
    }

    public void Exting() // 불 끄기 
    {
        Debug.Log(FireDistance);
        Debug.Log("Button clicked");

        if (ExtingScore > 0 && FireDistance < 10f)
        {
            //Fire.SetActive(false);
            Destroy(Fire);
            //StartCoroutine("FireFade");
            ExtingScore--;
            Fire = null;
            FireDistance = 10.0f;
            SetExtingText();
        }
        else if (FireDistance > 10f)
        {
            Debug.Log("소화기 사용 거리는 2m입니다. 가까이 가세요");
        }
    }

    public void SetExtingText() // 소화기 개수
    {
        ExtingText.text = ExtingScore.ToString();
    }

   /* IEnumerator FireFade()
   {
        Color startColor = Fire.color;

        for (int i = 0; i <100; i ++)
        {
            startColor.a = startColor.a - 0.01f;
            Fire.color = startColor;
            yield return new WaitForSeconds(0.01f);
        }
   } */
}