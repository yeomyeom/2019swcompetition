﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Box : MonoBehaviour
{
    public Text BoxText;
    static int BoxScore = 0;

    public Text CatText;
    static int CatScore = 0;

    public GameObject Dragon;
    public GameObject Cat;

    float CatDistance;

    public Image Success;
    public Image Again;

    // Start is called before the first frame update
    void Start()
    {
        Success.gameObject.SetActive(false);
        Again.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        CatDistance = Vector3.Distance(Dragon.transform.position, Cat.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Box") // 구급상자 먹었을 때 
        {
            other.gameObject.SetActive(false);

            BoxScore++;
            SetBoxText();
        }

        if (other.tag == "Door")
        {
            Debug.Log("OK");

            if (CatScore > 0)
            {
                Success.gameObject.SetActive(true);
                other.gameObject.SetActive(false);

                StartCoroutine("LoadEndGame");
            }
            else
            {
                StartCoroutine("GameAgain");
            }
        }

    }

    public void CatSave() // 고양이 구하기 
    {
        if (BoxScore > 0 && null != GameObject.Find("Cat"))
        {
            if (CatDistance < 5f)
            {
                Cat.gameObject.SetActive(false);

                BoxScore--;
                SetBoxText();

                CatScore++;
                SetCatText();
            }
        }
    }

    public void SetBoxText() // 구급상자 개수
    {
        BoxText.text = BoxScore.ToString();
    }

    public void SetCatText()
    {
        CatText.text = CatScore.ToString();
    }

    IEnumerator GameAgain()
    {
        Again.gameObject.SetActive(true);

        yield return new WaitForSeconds(2f);

        Again.gameObject.SetActive(false);

    }

    IEnumerator LoadEndGame()
    {
        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene("EndScene");

    }
}