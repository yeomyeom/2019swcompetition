﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartScene()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("LevelScene");
    }

    public void Floor1()
    {
        SceneManager.LoadScene("1Floor");
    }

    public void EndGame()
    {
        Application.Quit();
    }
}
